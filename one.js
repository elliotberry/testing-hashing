var crypto = require("crypto");
var fs = require("fs");
var XXHash = require("xxhash");
var HashStream = require("xxhash").Stream;
const crc = require("crc");
let filePath = "example.txt";
const MurmurHash3 = require("imurmurhash");
var murmur = require('murmurhash-native/stream');
const hashy = crypto.createHash('sha256');
const ggg = crypto.createHash('md5');
const bb = crypto.createHash('blake2s256');
/*

[
    'RSA-MD4',
    'RSA-MD5',
    'RSA-MDC2',
    'RSA-RIPEMD160',
    'RSA-SHA1',
    'RSA-SHA1-2',
    'RSA-SHA224',
    'RSA-SHA256',
    'RSA-SHA3-224',
    'RSA-SHA3-256',
    'RSA-SHA3-384',
    'RSA-SHA3-512',
    'RSA-SHA384',
    'RSA-SHA512',
    'RSA-SHA512/224',
    'RSA-SHA512/256',
    'RSA-SM3',
    'blake2b512',
    'blake2s256',
    'id-rsassa-pkcs1-v1_5-with-sha3-224',
    'id-rsassa-pkcs1-v1_5-with-sha3-256',
    'id-rsassa-pkcs1-v1_5-with-sha3-384',
    'id-rsassa-pkcs1-v1_5-with-sha3-512',
    'md4',
    'md4WithRSAEncryption',
    'md5',
    'md5-sha1',
    'md5WithRSAEncryption',
    'mdc2',
    'mdc2WithRSA',
    'ripemd',
    'ripemd160',
    'ripemd160WithRSA',
    'rmd160',
    'sha1',
    'sha1WithRSAEncryption',
    'sha224',
    'sha224WithRSAEncryption',
    'sha256',
    'sha256WithRSAEncryption',
    'sha3-224',
    'sha3-256',
    'sha3-384',
    'sha3-512',
    'sha384',
    'sha384WithRSAEncryption',
    'sha512',
    'sha512-224',
    'sha512-224WithRSAEncryption',
    'sha512-256',
    'sha512-256WithRSAEncryption',
    'sha512WithRSAEncryption',
    'shake128',
    'shake256',
    'sm3',
    'sm3WithRSAEncryption',
    'ssl3-md5',
    'ssl3-sha1',
    'whirlpool'
  ]
*/


let native = function() { //1.4ms
    const input = fs.createReadStream(filePath);
    input.pipe(hashy).pipe(process.stdout);
}
let native2 = function() { //.84ms
    var readStream = fs.createReadStream(filePath);
  
    readStream.on("data", function(chunk) {
        hashy.update(chunk);
    }).on("end", function() {
        console.log(hashy.digest("hex"));
    });
}
let native3 = function() { //.96ms
    var readStream = fs.createReadStream(filePath);
  
    readStream.on("data", function(chunk) {
        bb.update(chunk);
    }).on("end", function() {
        console.log(bb.digest("hex"));
    });
}
let a = function() {
    var file = fs.readFileSync(filePath);
    var result = XXHash.hash(file, 0xcafebabe);
    console.log(result);
};

let b = function() {
    //1.5 - 1.6ms
    var readStream = fs.createReadStream(filePath);
    var hash = crypto.createHash("sha1");
    readStream.on("data", function(chunk) {
        hash.update(chunk);
    }).on("end", function() {
        console.log(hash.digest("hex"));
    });
};

let c = function() {
    //1.4ms
    var hasher = new XXHash(0xcafebabe);

    fs.createReadStream(filePath).on("data", function(data) {
        hasher.update(data);
    }).on("end", function() {
        // console.log(hasher.digest());
    });
};

let d = function() {
    // 7.6 - 8ms
    var hasher = new HashStream(0xcafebabe);

    fs.createReadStream(filePath).pipe(hasher).on("finish", function() {
        // console.log(hasher.read());
    });
};

var elapsed_time = function(start, note) {
    var precision = 5; // 3 decimal places
    var elapsed = process.hrtime(start)[1] / 1000000; // divide by a million to get nano to milli
    console.log(process.hrtime(start)[0] + " s, " + elapsed.toFixed(precision) + " ms - " + note); // print message + time
    start = process.hrtime(); // reset the timer
};

let measure = function(cb, arg) {
    var start = process.hrtime();
    cb(arg);
    console.log(elapsed_time(start, "Function " + arg + " "));
};

let crcc = function() {
    let g = crc.crc32(fs.readFileSync(filePath, "utf8")).toString(16);
};

let mur = function() { //1.5
    var hashState = MurmurHash3("string");

    fs.createReadStream(filePath).on("data", function(data) {
        hashState.hash(data.toString());
    }).on("end", function() {
        console.log(hashState.result());
    });
};

let k = function() {
    var hash = murmur.createHash('murmurhash32', {seed: 123, encoding: 'hex'});
fs.createReadStream(filePath).pipe(hash).pipe(process.stdout);
}

var hashesArr = [
    'RSA-MD4',
    'RSA-MD5',
    'RSA-MDC2',
    'RSA-RIPEMD160',
    'RSA-SHA1',
    'RSA-SHA1-2',
    'RSA-SHA224',
    'RSA-SHA256',
    'RSA-SHA3-224',
    'RSA-SHA3-256',
    'RSA-SHA3-384',
    'RSA-SHA3-512',
    'RSA-SHA384',
    'RSA-SHA512',
    'RSA-SHA512/224',
    'RSA-SHA512/256',
    'RSA-SM3',
    'blake2b512',
    'blake2s256',
    'id-rsassa-pkcs1-v1_5-with-sha3-224',
    'id-rsassa-pkcs1-v1_5-with-sha3-256',
    'id-rsassa-pkcs1-v1_5-with-sha3-384',
    'id-rsassa-pkcs1-v1_5-with-sha3-512',
    'md4',
    'md4WithRSAEncryption',
    'md5',
    'md5-sha1',
    'md5WithRSAEncryption',
    'mdc2',
    'mdc2WithRSA',
    'ripemd',
    'ripemd160',
    'ripemd160WithRSA',
    'rmd160',
    'sha1',
    'sha1WithRSAEncryption',
    'sha224',
    'sha224WithRSAEncryption',
    'sha256', //.96ms
    'sha256WithRSAEncryption',
    'sha3-224', //1ms
    'sha3-256',
    'sha3-384',
    'sha3-512',
    'sha384',
    'sha384WithRSAEncryption',
    'sha512',
    'sha512-224',
    'sha512-224WithRSAEncryption',
    'sha512-256',
    'sha512-256WithRSAEncryption',
    'sha512WithRSAEncryption',
    'shake128', //.95ms
    'shake256',
    'sm3',
    'sm3WithRSAEncryption',
    'ssl3-md5',
    'ssl3-sha1',
    'whirlpool'
  ];

let anyNative = function(type) {
    const nn = crypto.createHash(type);
    var readStream = fs.createReadStream(filePath);
    readStream.on("data", function(chunk) {
        nn.update(chunk);
    }).on("end", function() {
       // console.log(nn.digest("hex"));
    });
}


async function f(theHash) {
await measure(anyNative, theHash);
} 

  
f('RSA-RIPEMD160');

